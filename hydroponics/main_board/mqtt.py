import logging
import json
import paho.mqtt.client as mqtt
from settings import ID, MQTT_CLIENT_NAME, MQTT_SERVER_IP, MQTT_SERVER_PASSWORD, MQTT_SERVER_PORT, MQTT_SERVER_USERNAME

logger = logging.getLogger(__name__)


class MQTT:
    """
    Class handling MQTT connection.
    """
    START_TOPIC = f"start/industrial_hydroponic/{ID}"
    DEAD_TOPIC = f"dead/industrial_hydroponic/{ID}"
    MODULE_TOPIC = f"industrial_hydroponic/{ID}"
    DATA_TOPIC = f"industrial_hydroponic/data/{ID}"

    DUMP_MESSAGE = json.dumps({"id": ID})
    def __init__(self, on_message=None):
        if on_message is None:
            on_message = self.on_message
        self.mqtt_client = mqtt.Client("test")
        self.mqtt_client.username_pw_set(MQTT_SERVER_USERNAME, password=MQTT_SERVER_PASSWORD)
        self.mqtt_client.connect(MQTT_SERVER_IP, int(MQTT_SERVER_PORT))
        self.mqtt_client.will_set(self.DEAD_TOPIC, self.DUMP_MESSAGE)
        self.mqtt_client.on_message = on_message
        self.mqtt_client.on_connect = self.on_connect
        self._publish(self.START_TOPIC, self.DUMP_MESSAGE)


    def send_measurement(self, message='{}'):
        self._publish(self.DATA_TOPIC, message)

    def connect_to_topic(self, topic: str = 'test'):
        self.mqtt_client.subscribe(topic)
        self.mqtt_client.loop_forever()

    @staticmethod
    def on_connect(client, userdata, flags, rc):
        print("MQTT connected with result code %s", str(rc))

    @staticmethod
    def on_message(client, userdata, message):
        print("Incoming MQTT message on %s : %s", message.topic, str(message.payload))

    def _publish(self, topic: str, payload = None):
        return self.mqtt_client.publish(topic, payload=payload)

if __name__ == '__main__':
    mqtt = MQTT()

