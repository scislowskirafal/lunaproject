import json
import time

import serial


class USBController:
    def __init__(self):
        self.ser = serial.Serial(port='/dev/ttyUSB0', baudrate=9600)

        self.first_peristaltic_pump_state = False
        self.second_peristaltic_pump_state = False
        self.third_peristaltic_pump_state = False
        self.fourth_peristaltic_pump_state = False

        self.main_pump_state = False

        self.first_relay_state = False
        self.second_relay_state = False

    def get_sensors_data(self):
        data_str = bytes('{"data": true}', 'utf-8')
        self.ser.write(data_str)
        data = ""
        time.sleep(1)
        while self.ser.inWaiting() > 0:
            data += self.ser.read().decode()
        return data

    def test_get_sensors_data(self):
        return {"ec": 2.41, "tds": 800.56, "ph": 6.54, "low_water_lvl": True, "high_water_lvl": False, "od": 200.23,
                "temperature": 22.4}

    def send_message(self, data: dict):
        self.ser.write(bytes(json.dumps(data), 'utf-8'))

    def calibrate_ph(self, type: str = "neutral"):
        if type == "acid" or type == "neutral":
            data = {"calibrate": f"ph_{type}"}
            self.send_message(data)

    def calibrate_ec(self, type: str = "low"):
        if type == "low" or type == "high":
            data = {"calibrate": f"ec_{type}"}
            self.send_message(data)

    def first_peristaltic_pump(self, state: bool):
        data = {"first_pump": state}
        self.send_message(data)
        self.first_peristaltic_pump_state = state

    def second_peristaltic_pump(self, state: bool):
        data = {"second_pump": state}
        self.send_message(data)
        self.second_peristaltic_pump_state = state

    def third_peristaltic_pump(self, state: bool):
        data = {"third_pump": state}
        self.send_message(data)
        self.third_peristaltic_pump_state = state

    def fourth_peristaltic_pump(self, state: bool):
        data = {"fourth_pump": state}
        self.send_message(data)
        self.fourth_peristaltic_pump_state = state

    def main_pump(self, state: bool):
        data = {"pump": state}
        self.send_message(data)
        self.main_pump_state = state

    def first_relay(self, state: bool):
        data = {"first_relay": state}
        self.send_message(data)
        self.first_relay_state = state

    def second_relay(self, state: bool):
        data = {"second_relay": state}
        self.send_message(data)
        self.second_relay_state = state

    def change_first_peristaltic_pump_state(self):
        data = {"first_pump": not self.first_peristaltic_pump_state}
        self.send_message(data)
        self.first_peristaltic_pump_state = not self.first_peristaltic_pump_state

    def change_second_peristaltic_pump_state(self):
        data = {"second_pump": not self.second_peristaltic_pump_state}
        self.send_message(data)
        self.second_peristaltic_pump_state = not self.second_peristaltic_pump_state

    def change_third_peristaltic_pump_state(self):
        data = {"third_pump": not self.third_peristaltic_pump_state}
        self.send_message(data)
        self.third_peristaltic_pump_state = not self.third_peristaltic_pump_state

    def change_fourth_peristaltic_pump_state(self):
        data = {"fourth_pump": not self.fourth_peristaltic_pump_state}
        self.send_message(data)
        self.fourth_peristaltic_pump_state = not self.fourth_peristaltic_pump_state

    def change_main_pump_state(self):
        data = {"pump": not self.main_pump_state}
        self.send_message(data)
        self.main_pump_state = not self.main_pump_state

    def change_first_relay_state(self):
        data = {"first_relay": not self.first_relay_state}
        self.send_message(data)
        self.first_relay_state = not self.first_relay_state

    def change_second_relay_state(self):
        data = {"second_relay": not self.second_relay_state}
        self.send_message(data)
        self.second_relay_state = not self.second_relay_state
