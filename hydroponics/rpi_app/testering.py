"""import json
path = "lunaraspberry/hydroponics/rpi_app/settings.json"


with open(path) as json_file:
    data = json.load(json_file)
    print(data['labels'])
    for dict in data['labels']:
        for key, value in dict.items():
            if key=='label1':
                print(value)"""
"""from tkinter import*

class MyGUI:
  def __init__(self):
    self.__mainWindow = Tk()
    #self.fram1 = Frame(self.__mainWindow)
    self.labelText = 'Enter amount to deposit'
    self.depositLabel = Label(self.__mainWindow, text = self.labelText)
    self.depositEntry = Entry(self.__mainWindow, width = 10)
    self.depositEntry.bind('<Return>', self.depositCallBack)
    self.depositLabel.pack()
    self.depositEntry.pack()

    mainloop()

  def depositCallBack(self,event):
    self.labelText = 'change the value'
    print(self.labelText)
    self.labelText['text'] = 'change the value'

myGUI = MyGUI
MyGUI()"""
try:
    import tkinter as tk  # python 3
    from tkinter import font as tkfont  # python 3
except ImportError:
    import Tkinter as tk  # python 2
    import tkFont as tkfont  # python 2


class SampleApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage, PageOne, PageTwo):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartPage")

    def show_frame(self, page_name):
        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="This is the start page", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)

        button1 = tk.Button(self, text="Go to Page One",
                            command=lambda: controller.show_frame("PageOne"))
        button2 = tk.Button(self, text="Go to Page Two",
                            command=lambda: controller.show_frame("PageTwo"))
        button1.pack()
        button2.pack()


class PageOne(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="This is page 1", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button = tk.Button(self, text="Go to the start page",
                           command=lambda: controller.show_frame("StartPage"))
        button.pack()


class PageTwo(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="This is page 2", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button = tk.Button(self, text="Go to the start page",
                           command=lambda: controller.show_frame("StartPage"))
        button.pack()


if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()

"""class MainPage(tk.Frame):
    def __init__(self, parent, controller, sc, mqtt):  # , **kwargs):
        # super(MainPage, self).__init__()
        self.controller = controller
        self.sc = sc
        self.mqtt = mqtt

        self.state1 = 0
        self.state2 = 0

        tk.Frame.__init__(self, parent)

        self.frame_menu = tk.Frame(self)
        self.frame_menu.grid(row=0, column=0)
        self.frame_options = tk.Frame(self)
        self.frame_options.grid(row=1, column=0)
        button_left = tk.Button(self.frame_menu, text="Włączniki", width=35, height=9,
                                command=self.button_left, bg='blue')
        button_left.grid(row=0, column=0, padx=10, pady=10)

        button_right = tk.Button(self.frame_menu, text="Czujniki", width=35, height=9,
                                 command=self.button_right, bg='yellow')
        button_right.grid(row=0, column=1, padx=10, pady=10)

        button_update = tk.Button(self.frame_menu, text="Update Czujników", width=35, height=9,
                                  command=self.button_update, bg='orange')
        button_update.grid(row=0, column=2, padx=10, pady=10)
        self.button1 = self.button2 = self.button3 = None
        self.label1 = self.label2 = self.label3 = self.label4 = self.label5 = self.label6 = None

    def add_sensor(self):
        self.sc.send_data(5)

    def button_left(self):
        for widget in self.frame_options.winfo_children():
            widget.destroy()
        self.button1 = tk.Button(self.frame_options, text="4 POMPY", width=70, height=6,
                                 command=lambda: self.mqtt.publish_data("testyyyyyyy/4pumps", "1"), bg="blue")
        self.button2 = tk.Button(self.frame_options, text="ŚWIATŁO", width=70, height=6,
                                 command=lambda: self.mqtt.publish_data("testyyyyyyy/light", "2"), bg="blue")
        self.button3 = tk.Button(self.frame_options, text="POMPA GŁÓWNA", width=70, height=6,
                                 command=lambda: self.mqtt.publish_data("testyyyyyyy/mainpump", "3"), bg="blue")
        self.button1.grid(row=0, column=0)
        self.button2.grid(row=1, column=0)
        self.button3.grid(row=2, column=0)

    def button_right(self):
        for widget in self.frame_options.winfo_children():
            widget.destroy()
        self.label1 = tk.Label(self.frame_options, text="1", width=35, height=9, bg="yellow")
        self.label2 = tk.Label(self.frame_options, text="2", width=35, height=9, bg="yellow")
        self.label3 = tk.Label(self.frame_options, text="3", width=35, height=9, bg="yellow")
        self.label4 = tk.Label(self.frame_options, text="4", width=35, height=9, bg="yellow")
        self.label5 = tk.Label(self.frame_options, text="5", width=35, height=9, bg="yellow")
        self.label6 = tk.Label(self.frame_options, text="6", width=35, height=9, bg="yellow")
        self.label1.grid(row=0, column=0)
        self.label2.grid(row=0, column=1)
        self.label3.grid(row=0, column=2)
        self.label4.grid(row=1, column=0)
        self.label5.grid(row=1, column=1)
        self.label6.grid(row=1, column=2)

    def read_settings(self, type:str, type_number:str, to_edit, new_text):
        if platform == "win32":
            path = r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\settings.json"
        else:
            path = "/home/pi/Desktop/lunaraspberry/timelapse/tests/photos/"
        with open(path) as json_file:
            data = json.load(json_file)
            for dict in data[type]:
                for key, value in dict.items():
                    if key == type_number:
                        print("self.label1['text']=new_text")

    def button_update(self):
        self.mqtt.publish_data("testyyyyyyy/update", "update")
        self.label1['text'] = "new_text"
        # path = "lunaraspberry/hydroponics/rpi_app/settings.json"
        self.read_settings("labels", "label1", self.label1, "siema")"""
# %%
# Import required libraries
from tkinter import *
from PIL import ImageTk, Image

# Create an instance of tkinter window
win = Tk()

# Define the geometry of the window
win.geometry("800x480")

frame = Frame(win, width=600, height=400)
frame.pack()
frame.place(anchor='center', relx=0.5, rely=0.5)

# Create an object of tkinter ImageTk
img = ImageTk.PhotoImage(Image.open(r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\img.png"))

# Create a Label Widget to display the text or Image
label = Label(frame, image=img)
label.pack()

win.mainloop()
# %%
import tkinter as tk
import random


class Example(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.ticker = tk.Text(height=1, wrap="none")
        self.ticker.pack(side="top", fill="x")

        self.ticker.tag_configure("up", foreground="green")
        self.ticker.tag_configure("down", foreground="red")
        self.ticker.tag_configure("event", foreground="black")

        self.data = ["AAPL", "GOOG", "MSFT"]
        self.after_idle(self.tick)

    def tick(self):
        symbol = self.data.pop(0)
        self.data.append(symbol)

        n = random.randint(-1, 1)
        tag = {-1: "down", 0: "even", 1: "up"}[n]

        self.ticker.configure(state="normal")
        self.ticker.insert("end", " %s %s" % (symbol, n), tag)
        self.ticker.see("end")
        self.ticker.configure(state="disabled")
        self.after(1000, self.tick)


if __name__ == "__main__":
    root = tk.Tk()
    Example(root).pack(fill="both", expand=True)
    root.mainloop()
# %%
from tkinter import *
from PIL import Image, ImageTk

root = Tk()

file = r"/hydroponics/rpi_app/nice_button.png"
image = Image.open(file)
display = ImageTk.PhotoImage(image)

label = Label(root, image=display, text="siema")
label.grid(row=0, column=0)

root.mainloop()
# %%
from tkinter import *
from tkinter import font

root = Tk()
root.title('Font Families')
fonts = list(font.families())
fonts.sort()


def populate(frame):
    '''Put in the fonts'''
    listnumber = 1
    for item in fonts:
        label = "listlabel" + str(listnumber)
        label = Label(frame, text=item, font=(item, 16)).pack()
        listnumber += 1


def onFrameConfigure(canvas):
    '''Reset the scroll region to encompass the inner frame'''
    canvas.configure(scrollregion=canvas.bbox("all"))


canvas = Canvas(root, borderwidth=0, background="#ffffff")
frame = Frame(canvas, background="#ffffff")
vsb = Scrollbar(root, orient="vertical", command=canvas.yview)
canvas.configure(yscrollcommand=vsb.set)

vsb.pack(side="right", fill="y")
canvas.pack(side="left", fill="both", expand=True)
canvas.create_window((4, 4), window=frame, anchor="nw")

frame.bind("<Configure>", lambda event, canvas=canvas: onFrameConfigure(canvas))

populate(frame)

root.mainloop()
#%%
from tkinter import *

# Create object
root = Tk()

# Adjust size
root.geometry("400x400")

# Create transparent window
# root.attributes('-alpha', 0.5)

# Execute tkinter
root.mainloop()