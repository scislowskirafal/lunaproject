import paho.mqtt.client as mqttClient
import time


class Mqtt:

    def __init__(self, ip="broker.hivemq.com", port=1883, user="scislowskirafal", password="1882000123aA"):
        self.broker_address = ip
        self.port = port
        self.user = user
        self.password = password

        self.client = mqttClient.Client("Python")  # create new instance
        self.client.username_pw_set(self.user, password=self.password)  # set username and password
        self.client.on_connect = self.on_connect  # attach function to callback
        self.client.on_message = self.on_message

    def connect(self):
        try:
            self.client.connect(self.broker_address, port=self.port)  # connect to broker

            self.client.loop_start()
            self.Connected = False  # global variable for the state of the connection
            while not self.Connected:  # Wait for connection
                time.sleep(0.1)
            self.publish_data("test", "Mqtt works")
        except:
            print("Cannot connect to broker")

    def publish_data(self, topic, data):  # Send data by mqtt
        self.client.publish(topic, data, qos=0)

    def read_data(self, topic):  # Recive data by mqtt
        return self.client.subscribe(topic)

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            # print("Connected to broker")
            self.Connected = True  # Signal connection

        else:
            print("Connection failed")

    def on_message(self, client, userdata, msg):
        print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
        print(msg.payload.decode("utf-8"))
        return msg.payload.decode("utf-8")


# a = Mqtt()
# a.connect()
# a.publish_data(topic="printer", data="C11")
# time.sleep(1)
# b = a.read_data("test")
"""{"buttons":[
  { "firstName":"John", "lastName":"Doe" },
  { "firstName":"Anna", "lastName":"Smith" },
  { "firstName":"Peter", "lastName":"Jones" }
]}"""