import tkinter as tk
import json
from sys import platform
import threading as th
import serial
import paho.mqtt.client as paho
from serialconnection import *
from tkinter import ttk
from tkinter import messagebox
#from PIL import ImageTk, Image

# Hue:227, Sat:240, Lum:227
# (227, 240, 227)
# Red:255, Green:227, Blue:236
# (255, 227, 236)
light_red = (255, 227, 236)
light_green = (196, 253, 203)
light_blue = (183, 221, 253)
LARGE_FONT = ("Verdana", 32)
width1 = 20
height1 = 6


class App(tk.Tk):

    def __init__(self, sc, mqtt, *args, **kwargs):
        # tk.Tk.__init__(self, *args, **kwargs)
        super().__init__(*args, **kwargs)
        tk.Tk.wm_title(self, "App for Database")
        #self.bgImage = tk.PhotoImage(file=r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\images\luna.png")
        #self.bgImageLabel = tk.Label(self, image=self.bgImage)
        #self.bgImageLabel.place(x=0, y=0)
        container = tk.Frame(self)
        container.grid(column=0, row=0)  # (side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        container.sc = sc
        container.mqtt = mqtt
        container.config(bg="#%02x%02x%02x" % light_red)  # , cursor='none')

        self.frames = {}

        for F in (StartPage, MainPage, PageOne, PageTwo):
            page_name = F.__name__
            frame = F(container, self, sc, mqtt)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")  # , sticky="nsew")
        self.show_frame("MainPage")

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.configure(bg="#%02x%02x%02x" % light_red)
        frame.tkraise()


class MainPage(tk.Frame):
    def __init__(self, parent, controller, sc, mqtt):
        tk.Frame.__init__(self, parent)
        #self.bgImage = tk.PhotoImage(file=r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\images\luna.png")
        #self.bgImageLabel = tk.Label(self, image=self.bgImage)
        #self.bgImageLabel.place(x=0, y=0)
        self.controller = controller
        self.sc = sc
        self.mqtt = mqtt
        self.frame = tk.Frame(self)
        self.frame.grid(row=0, columnspan=6)
        """label = tk.Label(self, text="This is the start page")  # , font=controller.title_font)
        label.grid(column=0, row=0)"""
        main_page = tk.Button(self.frame, text="MainPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                              command=lambda: controller.show_frame("MainPage"))
        start_page = tk.Button(self.frame, text="StartPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                               command=lambda: controller.show_frame("StartPage"))
        page_one = tk.Button(self.frame, text="PageOne", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageOne"))
        page_two = tk.Button(self.frame, text="PageTwo", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageTwo"))
        main_page.grid(row=0, column=0, padx=10, pady=10)
        start_page.grid(row=0, column=1, padx=10, pady=10)
        page_one.grid(row=0, column=2, padx=10, pady=10)
        page_two.grid(row=0, column=3, padx=10, pady=10)


class PageOne(tk.Frame):

    def __init__(self, parent, controller, sc, mqtt):
        tk.Frame.__init__(self, parent)
        #self.bgImage = tk.PhotoImage(file=r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\images\luna.png")
        #self.bgImageLabel = tk.Label(self, image=self.bgImage)
        #self.bgImageLabel.place(x=0, y=0)
        self.controller = controller
        self.sc = sc
        self.mqtt = mqtt
        self.frame = tk.Frame(self)
        self.frame.grid(row=0, columnspan=6)
        main_page = tk.Button(self.frame, text="MainPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                              command=lambda: controller.show_frame("MainPage"))
        start_page = tk.Button(self.frame, text="StartPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                               command=lambda: controller.show_frame("StartPage"))
        page_one = tk.Button(self.frame, text="PageOne", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageOne"))
        page_two = tk.Button(self.frame, text="PageTwo", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageTwo"))
        button_update = tk.Button(self.frame, text="Update Buttons", width=width1, height=height1,
                                  bg="#%02x%02x%02x" % light_green,
                                  command=lambda: (
                                      button1.config(text='change the value')
                                  ))
        main_page.grid(row=0, column=0, padx=10, pady=10)
        start_page.grid(row=0, column=1, padx=10, pady=10)
        page_one.grid(row=0, column=2, padx=10, pady=10)
        page_two.grid(row=0, column=3, padx=10, pady=10)
        button_update.grid(row=0, column=4, padx=10, pady=10)
        width2 = 35
        height2 = 4
        button1 = tk.Button(self, text="Pompa 1", width=width2, height=height2, padx=10, pady=10,
                            command=lambda: self.mqtt.publish_data("testyyyyyyy/4pumps", "1"),
                            bg="#%02x%02x%02x" % light_green)
        button2 = tk.Button(self, text="Pompa2", width=width2, height=height2, padx=10, pady=10,
                            command=lambda: self.mqtt.publish_data("testyyyyyyy/light", "2"),
                            bg="#%02x%02x%02x" % light_green)
        button3 = tk.Button(self, text="Pompa3", width=width2, height=height2, padx=10, pady=10,
                            command=lambda: self.mqtt.publish_data("testyyyyyyy/mainpump", "3"),
                            bg="#%02x%02x%02x" % light_green)
        button4 = tk.Button(self, text="Pompa4", width=width2, height=height2, padx=10, pady=10,
                            command=lambda: self.mqtt.publish_data("testyyyyyyy/4pumps", "1"),
                            bg="#%02x%02x%02x" % light_green)
        button5 = tk.Button(self, text="POMPA GŁÓWNA", width=width2, height=height2, padx=10, pady=10,
                            command=lambda: self.mqtt.publish_data("testyyyyyyy/light", "2"),
                            bg="#%02x%02x%02x" % light_green)
        button6 = tk.Button(self, text="ŚWIATŁO", width=width2, height=height2, padx=10, pady=10,
                            command=lambda: self.mqtt.publish_data("testyyyyyyy/mainpump", "3"),
                            bg="#%02x%02x%02x" % light_green)
        button7 = tk.Button(self, text="PRZEKAŹNIK", width=width2, height=height2, padx=10, pady=10,
                            command=lambda: self.mqtt.publish_data("testyyyyyyy/mainpump", "3"),
                            bg="#%02x%02x%02x" % light_green)
        button1.grid(row=2, column=0)
        button2.grid(row=3, column=0)
        button3.grid(row=4, column=0)
        button4.grid(row=5, column=0)
        button5.grid(row=2, column=1)
        button6.grid(row=3, column=1)
        button7.grid(row=4, column=1)

    def read_settings(self, type:str, type_number:str, to_edit, new_text):
        if platform == "win32":
            path = r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\settings.json"
        else:
            path = "/home/pi/Desktop/lunaraspberry/hydroponics/rpi_app/settings.json"
        with open(path) as json_file:
            data = json.load(json_file)
            for dict in data[type]:
                for key, value in dict.items():
                    if key == type_number:
                        print("self.label1['text']=new_text")


class PageTwo(tk.Frame):

    def __init__(self, parent, controller, sc, mqtt):
        tk.Frame.__init__(self, parent)
        #self.bgImage = tk.PhotoImage(file=r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\images\luna.png")
        #self.bgImageLabel = tk.Label(self, image=self.bgImage)
        #self.bgImageLabel.place(x=0, y=0)
        self.controller = controller
        self.sc = sc
        self.mqtt = mqtt
        self.frame = tk.Frame(self)
        self.frame.grid(row=0, columnspan=6)
        main_page = tk.Button(self.frame, text="MainPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                              command=lambda: controller.show_frame("MainPage"))
        start_page = tk.Button(self.frame, text="StartPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                               command=lambda: controller.show_frame("StartPage"))
        page_one = tk.Button(self.frame, text="PageOne", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageOne"))
        page_two = tk.Button(self.frame, text="PageTwo", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageTwo"))
        button_update = tk.Button(self.frame, text="Update Buttons", width=width1, height=height1,
                                  bg="#%02x%02x%02x" % light_green,
                                  command=lambda: (
                                      self.change_text(word_text=self.text1, word="word", description="naaura"),
                                      # self.text1.tag_configure("word", foreground="blue")
                                  ))
        main_page.grid(row=0, column=0, padx=10, pady=10)
        start_page.grid(row=0, column=1, padx=10, pady=10)
        page_one.grid(row=0, column=2, padx=10, pady=10)
        page_two.grid(row=0, column=3, padx=10, pady=10)
        button_update.grid(row=0, column=4)
        # file = r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\images\nice_button.png"
        # image = Image.open(file)
        # display = ImageTk.PhotoImage(image)
        width2 = 35
        height2 = 6
        self.text1 = tk.Text(self, width=width2, height=height2, bg="#%02x%02x%02x" % light_green)
        self.text2 = tk.Text(self, width=width2, height=height2, bg="#%02x%02x%02x" % light_green)
        self.text3 = tk.Text(self, width=width2, height=height2, bg="#%02x%02x%02x" % light_green)
        self.text4 = tk.Text(self, width=width2, height=height2, bg="#%02x%02x%02x" % light_green)
        self.text5 = tk.Text(self, width=width2, height=height2, bg="#%02x%02x%02x" % light_green)
        # self.text6 = tk.Text(self, width=width2, height=height2, bg="#%02x%02x%02x" % light_green)
        # self.text1=tk.Label(self, image=r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\images\nice_button.png")
        self.text1.grid(row=2, column=0)
        self.text2.grid(row=3, column=0)
        self.text3.grid(row=4, column=0)
        self.text4.grid(row=2, column=1)
        self.text5.grid(row=3, column=1)
        # self.text6.grid(row=3, column=2)
        self.set_text(word_text=self.text1, word="woda °C", description="siemaneczko")
        self.set_text(word_text=self.text2, word="TDS mg/l", description="siemaneczko")
        self.set_text(word_text=self.text3, word="EC mS/cm", description="siemaneczko")
        self.set_text(word_text=self.text4, word="tlen %", description="siemaneczko")
        self.set_text(word_text=self.text5, word="PH", description="siemaneczko")
        # self.set_text(word_text=self.text6, word="water level", description="siemaneczko")

    def read_settings(self, type:str, type_number:str, to_edit, new_text):
        if platform == "win32":
            path = r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\settings.json"
        else:
            path = "/home/pi/Desktop/lunaraspberry/timelapse/tests/photos/"
        with open(path) as json_file:
            data = json.load(json_file)
            for dict in data[type]:
                for key, value in dict.items():
                    if key == type_number:
                        print("self.label1['text']=new_text")

    @staticmethod
    def change_text(word_text, word, description):
        word_text.tag_configure(word, foreground="blue")

    @staticmethod
    def set_text(word_text, word, description):
        word_text.insert('end', word + '\n')
        word_text.tag_add('word', '1.0', '1.end')
        word_text.tag_config('word', font='arial 15 bold', spacing3=10)  # Set font, size and style
        word_text.insert('end', description)
        word_text.tag_add('description', '4.0', '99.end')
        word_text.tag_config('description', font='helvetica 12', lmargin1=15, lmargin2=15)
        word_text.config(state=tk.DISABLED)


class StartPage(tk.Frame):

    def __init__(self, parent, controller, sc, mqtt):
        tk.Frame.__init__(self, parent)
        #self.bgImage = tk.PhotoImage(file=r"D:\pythonProject\lunaraspberry\hydroponics\rpi_app\images\luna.png")
        #self.bgImageLabel = tk.Label(self, image=self.bgImage)
        #self.bgImageLabel.place(x=0, y=0)
        self.controller = controller
        self.sc = sc
        self.mqtt = mqtt

        self.frame = tk.Frame(self)
        self.frame.grid(row=0, columnspan=6)
        """label = tk.Label(self, text="This is the start page")  # , font=controller.title_font)
        label.grid(column=0, row=0)"""
        main_page = tk.Button(self.frame, text="MainPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                              command=lambda: controller.show_frame("MainPage"))
        start_page = tk.Button(self.frame, text="StartPage", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                               command=lambda: controller.show_frame("StartPage"))
        page_one = tk.Button(self.frame, text="PageOne", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageOne"))
        page_two = tk.Button(self.frame, text="PageTwo", width=width1, height=height1, bg="#%02x%02x%02x" % light_blue,
                             command=lambda: controller.show_frame("PageTwo"))
        # label.grid(column=0, row=0)
        main_page.grid(row=0, column=0, padx=10, pady=10)
        start_page.grid(row=0, column=1, padx=10, pady=10)
        page_one.grid(row=0, column=2, padx=10, pady=10)
        page_two.grid(row=0, column=3, padx=10, pady=10)
        self.combo_frame=tk.Frame(self, width=100, height=100)
        self.combo = ttk.Combobox(self.combo_frame)
        self.combo['values'] = ("COM1", "COM2", "COM3", "/dev/ttyAMA0", "/dev/ttyUSB0")
        self.combo.current(4)
        self.combo.grid(row=0, column=0)
        self.button = tk.Button(self, text="Enter", width=15, anchor='w',
                                command=lambda: self.connect_to_microcontroller())
        self.button_exit = tk.Button(self, text=r"\nExit\n", anchor='w', width=30, command=quit)
        self.combo_frame.grid(row=1, column=0)
        self.button.grid(row=2, column=0)
        self.button_exit.grid(row=3, column=0)

    def connect_to_microcontroller(self):
        try:
            port = self.combo.get()
            self.sc.connect(port)
            self.controller.show_frame(MainPage)
        except:
            messagebox.showinfo('Warning', 'There is no device to connect')
