import time
import threading as th
import json
import schedule
from main_board import USBController

# def send_information_to_server():
#     mqtt = MQTT()
#     data = {"ec": 2.41, "tds": 800.56, "ph": 6.54, "low_water_lvl": True, "high_water_lvl": False, "od": 200.23,
#                 "temperature": 22.4}
#     mqtt.send_measurement(json.dumps(data))
#     time.sleep(30)

class Test:

    def push_data(self):
        print("test")


def set_routines(main_board):
    schedule.every(30).minutes.do(main_board.change_main_pump_state)
    schedule.every().day.at("10:00").do(main_board.first_relay(True))
    schedule.every().day.at("22:00").do(main_board.first_relay(False))

    while True:
        schedule.run_pending()
        time.sleep(1)

if __name__ == '__main__':
    # main_board = USBController()
    main_board = Test()
    routines = th.Thread(target=set_routines, args=[main_board])
    routines.start()

    while True:
        time.sleep(1)
    # while True:
    #     print(main_board.get_sensors_data())
    #     print("test")
    #     time.sleep(5)
