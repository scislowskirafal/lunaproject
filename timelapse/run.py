from sys import platform
import paho.mqtt.client as paho

if platform == "win32":
    import timelapse.src.pc_gif_maker as gif
    import timelapse.src.pc_save_photos as photo

    fp_in = "lunaraspberry/timelapse/tests/photos/"
    fp_out = "lunaraspberry/timelapse/tests/gifs/"
else:
    import src.pc_gif_maker as gif
    import src.pc_save_photos as photo

    fp_in = "timelapse/tests/photos/"
    fp_out = "timelapse/tests/gifs/"


def timelapse(period: int, n_photos: int, n_camera: int, interval: int):
    """
    :param period: int seconds
    :param interval: int miliseconds
    :return:
    """
    clock = photo.SavePhotos(period=period, n_photos=n_photos, n_camera=n_camera)
    clock.photo_series(infinite=True)
    my_gif = gif.MakeAGIF(fp_in=fp_in, fp_out=fp_out)
    my_gif.make_a_gif(duration=interval)


def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_message(client, userdata, msg):
    timelapse(period=1, n_photos=3, n_camera=0, interval=200)
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


if __name__ == '__main__':
    # timelapse(duration=2, n_photos=2, n_camera=0, interval=200)
    client = paho.Client()
    client.on_subscribe = on_subscribe
    client.on_message = on_message
    client.connect("broker.hivemq.com", 1883)
    client.subscribe("testyyyyyyy/test0", qos=1)

    client.loop_forever()
