from sys import platform
if platform == "win32":
    import timelapse.src.pc_gif_maker as gif
    import timelapse.src.pc_save_photos as photo

    fp_in = "lunaraspberry/timelapse/tests/photos/"
    fp_out = "lunaraspberry/timelapse/tests/gifs/"
else:
    import src.pc_gif_maker as gif
    import src.pc_save_photos as photo

    fp_in = "/home/pi/Desktop/lunaraspberry/timelapse/tests/photos/"
    fp_out = "/home/pi/Desktop/lunaraspberry/timelapse/tests/gifs/"
my_gif = gif.MakeAGIF(fp_in=fp_in, fp_out=fp_out)
my_gif.make_a_gif(duration=50)
