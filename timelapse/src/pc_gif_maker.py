import glob
from PIL import Image
import os
import re
from sys import platform
if platform == "win32":
    fp_in = "lunaraspberry/timelapse/tests/photos/"
    fp_out = "lunaraspberry/timelapse/tests/gifs/"
else:
    fp_in = "tests/photos/"
    fp_out = "tests/gifs/"

def sorted_alphanumeric(data):
    """
    Natural sorting of filenames eg:
    ['f170.jpg', 'f027.jpg', 'f003.jpg']

    :param data: this is a first param
    :returns: returns sorted list of filenames
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


class MakeAGIF:
    def __init__(self, fp_in, fp_out):
        """
        Makes a GIF from saved photos and save to 'save' folder.

        :param fp_in: input folder of photos
        :param fp_out: output folder of gifs
        """
        self.fp_in = fp_in
        self.fp_out = fp_out
        self.ii=0

    def last_number(self):
        """
        Return a natural number from name of last photo.

        :returns: photo of biggest index number (last photo)
        """
        i = 0
        if os.listdir(self.fp_out):
            i = os.listdir(self.fp_out)[-1]
            i = i[:-4]
            i = i[4:]
            i = int(i) + 1
        return i

    def grouped(self, iterable, n):
        "s -> (s0,s1,s2,...sn-1), (sn,sn+1,sn+2,...s2n-1), (s2n,s2n+1,s2n+2,...s3n-1), ..."
        return zip(*[iter(iterable)] * n)

    def make_a_gif(self, duration):
        """
        Make a GIF and saves it to folder save.
        Creates a folder 'save' if it does not exist.


        :param duration: this is a first param
        :returns: this is a description of what is returned
        :raises keyError: raises an exception
        """
        is_exist_in = os.path.exists(self.fp_in)
        if not is_exist_in:
            os.makedirs(self.fp_in)
        is_exist_out = os.path.exists(self.fp_out)
        if not is_exist_out:
            os.makedirs(self.fp_out)

        if os.listdir(self.fp_in):
            i = str(self.last_number())
            if os.listdir(self.fp_out):
                photo = sorted_alphanumeric(os.listdir(self.fp_out))
                photo = photo[-1]
                photo = photo[:-4]
                photo = photo[4:]
                i = int(photo) + 1
            self.fp_in += "*.jpg"
            print(i)
            photos_iter=sorted_alphanumeric(glob.glob(self.fp_in))
            img, *imgs = [Image.open(f) for f,_,_,_,_,_,_,_ in self.grouped(photos_iter, 8)]
            img.save(fp=self.fp_out + "film" + str(i) + ".gif", format='GIF', append_images=imgs,
                     save_all=True, duration=duration, loop=0)


if __name__ == '__main__':
    """
    Just makes GIFs.
    """
    my_gif = MakeAGIF(fp_in=fp_in, fp_out=fp_out)
    my_gif.make_a_gif(duration=100)
