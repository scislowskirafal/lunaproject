import time
from sys import platform
import os
import re
import cv2
from datetime import datetime
import datetime
# from threading import Thread

if platform == "win32":
    folder = "lunaraspberry/timelapse/tests/photos/"
else:
    folder = "/home/pi/Desktop/lunaraspberry/timelapse/tests/photos/"


def sorted_alphanumeric(data):
    """
    Natural sorting of filenames eg:
    ['f170.jpg', 'f027.jpg', 'f003.jpg']

    :param data: this is a first param
    :returns: returns sorted list of filenames
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


class SavePhotos:
    def __init__(self, period: int, n_photos: int, n_camera=0):
        """
        Class taking photos and saving them in photos folder.

        :param period: period between photos
        :param n_photos: n of photos
        :param n_camera: number of camera or path video path
        """
        self.start = time.time()
        self.n_photos = n_photos
        self.n_camera = n_camera
        self.period = period
        # self.thread = Thread(target=self.photo_series)

    def photo_series(self, infinite: bool, limit=1000):
        """
        Takes infinite n_photos in duration

        :returns: this is a description of what is returned
        :raises keyError: raises an exception
        """
        is_exist = os.path.exists(folder)

        if not is_exist:
            os.makedirs(folder)
        if infinite:
            self.n_photos = limit
        for i in range(self.n_photos):
            self.cap = cv2.VideoCapture(self.n_camera)
            ret, img = self.cap.read()
            if ret:
                self.photo(i, folder, img)
                time.sleep(self.period)
        self.cap.release()

    def photo(self, i, folder, img):
        """
        Function saving photo to special folder.

        :param i: iteration, next photo
        :param folder: path to photos folder
        """
        img_name = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M')
        img_text1 = datetime.datetime.now().strftime('%Y-%m-%d')
        img_text2 = datetime.datetime.now().strftime('%H:%M')
        print(img_name)
        now_hour = datetime.datetime.now().hour
        if now_hour > 9 and now_hour < 23:
            cv2.putText(img, img_text1, (10, 20), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 3)
            cv2.putText(img, img_text2, (10, 45), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 255), 3)
            cv2.imwrite(folder + "f" + str(img_name) + ".jpg", img)


if __name__ == "__main__":
    """
    Just saves photos.
    """
    clock = SavePhotos(period=15*60, n_photos=20, n_camera=0)
    clock.photo_series(infinite=True, limit=1000000)
