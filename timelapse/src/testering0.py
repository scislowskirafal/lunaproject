try:
    import Tkinter as tkinter
    import ttk

except:
    import tkinter
    import tkinter.ttk as ttk

root = tkinter.Tk()
root.geometry('%sx%s' % (root.winfo_screenwidth(), root.winfo_screenheight()))
root.pack_propagate(0)
textarea = tkinter.Text(root)

style = ttk.Style()
style.layout('Vertical.TScrollbar', [
    ('Vertical.Scrollbar.trough', {'sticky': 'nswe', 'children': [
        ('Vertical.Scrollbar.uparrow', {'side': 'top', 'sticky': 'nswe'}),
        ('Vertical.Scrollbar.downarrow', {'side': 'bottom', 'sticky': 'nswe'}),
        ('Vertical.Scrollbar.thumb', {'sticky': 'nswe', 'unit': 1, 'children': [
            ('Vertical.Scrollbar.grip', {'sticky': ''})
            ]})
        ]})
    ])

scrollbar = ttk.Scrollbar(root, command=textarea.yview)
textarea.config(yscrollcommand=scrollbar.set)


textarea.pack(side='left', fill='both', expand=0)
scrollbar.pack(side='left', fill='both', expand=1)

root.mainloop()