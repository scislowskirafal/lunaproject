from sys import platform
if platform == "win32":
    import timelapse.src.pc_gif_maker as gif
    import timelapse.src.pc_save_photos as photo

    fp_in = "lunaraspberry/timelapse/tests/photos/"
    fp_out = "lunaraspberry/timelapse/tests/gifs/"
else:
    import src.pc_gif_maker as gif
    import src.pc_save_photos as photo

    fp_in = "/home/pi/Desktop/lunaraspberry/timelapse/tests/photos/"
    fp_out = "/home/pi/Desktop/lunaraspberry/timelapse/tests/gifs/"
clock = photo.SavePhotos(period=60*60, n_photos=1000000, n_camera=0)
clock.photo_series(infinite=True, limit=1000000)

"""
clock = photo.SavePhotos(period=3, n_photos=2, n_camera=0)
clock.photo_series(infinite=True, limit=2)
"""
"""
my_gif = gif.MakeAGIF(fp_in=fp_in, fp_out=fp_out)
my_gif.make_a_gif(duration=200)
"""
#uoz5lfxzoobjbcdxh7xvgzitznhbftlfv6pbhn5q4pahrx4steqeybqd.onion
